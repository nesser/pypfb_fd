# from pypfb.gui import pfb_gui
from pypfb.controller import Controller
import sys

if __name__ == "__main__":
        ctrl = Controller()
        ctrl.showMainWindow()
        sys.exit(ctrl.execute())
