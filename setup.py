import sys
from setuptools import setup, find_packages

setup(name="pypfb_fd",
    version='0.0.1',
    description='Polyphase Filterbank - Filter Design',
    author="Niclas Esser",
    author_email="nesser@mpifr-bonn.mpg.de",
    packages=find_packages(),
    install_requires=['numpy', 'scipy', 'matplotlib', 'pyqt5'])
