import numpy as np
import scipy.signal as ss
import matplotlib.pyplot as plt

def additiveSignals(generators):
    res = np.zeros(generators[0].samples)
    t = generators[0].t
    for gen in generators:
        if gen.used:
            res += gen.sample()
    return t, res

class SignalGenerator():

    def __init__(self, **kwargs):
        self.used = False
        self.update(kwargs)

    def __str__(self):
        s = "fs {}\n".format(self.fs)
        s += "duration {}\n".format(self.duration)
        s += "type {}\n".format(self.type)
        s += "dtype {}\n".format(self.dtype)
        s += "gain {}\n".format(self.gain)
        s += "offset {}\n".format(self.offset)
        # Sinus/cosine
        s += "freq {}\n".format(self.freq)
        s += "phase {}\n".format(self.phase)
        # Noise
        s += "mean {}\n".format(self.mean)
        s += "deviation {}\n".format(self.deviation)
        s += "noise_type {}\n".format(self.noise_type)
        # Sweep
        s += "f0 {}\n".format(self.f0)
        s += "f1 {}\n".format(self.f1)
        s += "method {}\n".format(self.method)
        return s

    def isUsed(self):
        return self.used
    def setUsed(self, val):
        self.used = val

    def sample(self):
        return getattr(self, self.type)()

    def sinus(self):
        y = np.zeros(len(self.t))
        for f in self.freq:
            y += self.gain * np.sin(2 * np.pi * f * self.t + self.phase)# + self.offset
        return y

    def cosine(self):
        y = np.zeros(len(self.t))
        for f in self.freq:
            y += self.gain * np.cos(2 * np.pi * f * self.t + self.phase) + self.offset
        return y

    def noise(self):
        return self.gain * np.random.normal(self.mean, self.deviation, self.samples) + self.offset

    def sweep(self):
        return self.gain * ss.chirp(self.t, f0=self.f0, f1=self.f1, t1=self.period, method=self.method) + self.offset

    # def rectangle(self):
    #     return self.gain * np.where(self.t < self.period, 1, 0) + self.offset
    # def dirac(self):
    #     return self.gain * np.where(self.t < self.period, 1, 0) + self.offset

    # def sawtooth(self):
    #     return self.gain * signal.sawtooth(2 * np.pi * self.period * self.t)

    def getParams(self):
        params = {}
        params['freq'] = self.freq
        params['phase'] = self.phase
        params['noise_type'] = self.noise_type
        params['mean'] = self.mean
        params['devition'] = self.deviation
        params['f0'] = self.f0
        params['f1'] = self.f1
        params['sweep_method'] = self.method
        return params

    def update(self, kwargs):
        if 'kwargs' in kwargs:
            kwargs = kwargs['kwargs']
        self.fs = kwargs.get('fs',32e3)
        self.duration = kwargs.get('duration', 1)
        self.type = kwargs.get('type', 'sinus')
        self.dtype = kwargs.get('dtype', 'int8')
        self.gain = kwargs.get('gain', 1.0)
        self.offset = kwargs.get('offset', 0)
        # Sinus/cosine
        self.freq = kwargs.get('freq', [self.fs/4])
        self.phase = kwargs.get('phase', 0)
        # Noise
        self.noise_type = kwargs.get("noise_type", "gaussian")
        self.mean = kwargs.get('mean', 0.0)
        self.deviation = kwargs.get('deviation', 1.0)
        # Sweep
        self.f0 = kwargs.get('f0', self.fs / 8)
        self.f1 = kwargs.get('f1', self.fs / 4)
        self.method = kwargs.get('sweep_method', 'linear')
        self.period = kwargs.get('period', self.duration)

        self.samples = int(self.fs * self.duration)
        self.t = np.linspace(0, self.duration, self.samples)#, endpoint=True, retstep=True))
        self.output = np.zeros(self.samples)
