# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pypfb/gui/qt_ui/noise_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_NoiseDialog(object):
    def setupUi(self, NoiseDialog):
        NoiseDialog.setObjectName("NoiseDialog")
        NoiseDialog.resize(375, 240)
        self.gridLayout = QtWidgets.QGridLayout(NoiseDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(NoiseDialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_6 = QtWidgets.QLabel(NoiseDialog)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 1, 0, 1, 1)
        self.mean_value_input = QtWidgets.QLineEdit(NoiseDialog)
        self.mean_value_input.setObjectName("mean_value_input")
        self.gridLayout_2.addWidget(self.mean_value_input, 1, 1, 1, 1)
        self.noise_type_input = QtWidgets.QComboBox(NoiseDialog)
        self.noise_type_input.setObjectName("noise_type_input")
        self.noise_type_input.addItem("")
        self.gridLayout_2.addWidget(self.noise_type_input, 0, 1, 1, 1)
        self.label_7 = QtWidgets.QLabel(NoiseDialog)
        self.label_7.setObjectName("label_7")
        self.gridLayout_2.addWidget(self.label_7, 2, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(NoiseDialog)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 0, 0, 1, 1)
        self.deviation_value_input = QtWidgets.QLineEdit(NoiseDialog)
        self.deviation_value_input.setObjectName("deviation_value_input")
        self.gridLayout_2.addWidget(self.deviation_value_input, 2, 1, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem)
        self.apply_button = QtWidgets.QPushButton(NoiseDialog)
        self.apply_button.setObjectName("apply_button")
        self.horizontalLayout_8.addWidget(self.apply_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem1)
        self.gridLayout_3.addLayout(self.horizontalLayout_8, 0, 2, 1, 1)
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem2)
        self.cancel_button = QtWidgets.QPushButton(NoiseDialog)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout_13.addWidget(self.cancel_button)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem3)
        self.gridLayout_3.addLayout(self.horizontalLayout_13, 0, 0, 1, 1)
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_14.addItem(spacerItem4)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_14.addItem(spacerItem5)
        self.gridLayout_3.addLayout(self.horizontalLayout_14, 0, 1, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_3, 2, 0, 1, 1)

        self.retranslateUi(NoiseDialog)
        QtCore.QMetaObject.connectSlotsByName(NoiseDialog)

    def retranslateUi(self, NoiseDialog):
        _translate = QtCore.QCoreApplication.translate
        NoiseDialog.setWindowTitle(_translate("NoiseDialog", "Dialog"))
        self.label.setText(_translate("NoiseDialog", "<html><head/><body><p><span style=\" font-weight:600;\">Signal Configuration</span></p><p>Choose signal specifications</p></body></html>"))
        self.label_6.setText(_translate("NoiseDialog", "Mean"))
        self.mean_value_input.setText(_translate("NoiseDialog", "0"))
        self.noise_type_input.setItemText(0, _translate("NoiseDialog", "Gaussian"))
        self.label_7.setText(_translate("NoiseDialog", "Deviation"))
        self.label_5.setText(_translate("NoiseDialog", "Type"))
        self.deviation_value_input.setText(_translate("NoiseDialog", "1"))
        self.apply_button.setText(_translate("NoiseDialog", "Apply"))
        self.cancel_button.setText(_translate("NoiseDialog", "Cancel"))
