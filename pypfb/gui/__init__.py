from PyQt5.QtWidgets import QApplication
from pypfb.gui.main_window import Ui_MainWindow
from pypfb.gui.sincos_dialog import Ui_SinCosDialog
from pypfb.gui.noise_dialog import Ui_NoiseDialog
from pypfb.gui.sweep_dialog import Ui_SweepDialog
from PyQt5.QtWidgets import QFileDialog
from PyQt5 import QtWidgets as qtw
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qtagg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import numpy as np

def createDialog(type, params=None):
    if type == "sinus" or type == "cosine":
        dialog = WindowWrapper(Ui_SinCosDialog)
    elif type == "noise":
        dialog = WindowWrapper(Ui_NoiseDialog)
    elif type == "sweep":
        dialog = WindowWrapper(Ui_SweepDialog)
    elif type == "file_dialog":
        dialog = QFileDialog()
        dialog.setObjectName("Files")
        # dialog.setNameFilters("Numpy files: (*.np *.npz)")
        dialog.resize(400,400)
        # dialog = WindowWrapper(Ui_FileDialog)
    else:
        dialog = WindowWrapper(Ui_SinCosDialog)
    return dialog

def getDialogParams(dialog):
    params = {}
    if isinstance(dialog,str):
        window = createDialog(type)
        dialog = window.main
    if isinstance(dialog, Ui_SinCosDialog):
        params['freq'] = np.asarray(dialog.frequencies_input.text().split(','), dtype=np.float32)
        params['phase'] = float(dialog.phase_offset_input.text())
    elif isinstance(dialog, Ui_NoiseDialog):
        params['noise_type'] = dialog.noise_type_input.currentText()
        params['mean'] = float(dialog.mean_value_input.text())
        params['deviation'] = float(dialog.deviation_value_input.text())
    elif isinstance(dialog, Ui_SweepDialog):
        params['sweep_method'] = dialog.sweep_method_input.currentText()
        params['f0'] = float(dialog.f0_input.text())
        params['f1'] = float(dialog.f1_input.text())
    elif isinstance(dialog, QFileDialog):
        return params
    else:
        return params
    return params

def setDialogParams(dialog, params):
    if isinstance(dialog, Ui_SinCosDialog):
        freq = ""
        for i in range(len(params['freq'])):
            freq += str(params['freq'][i])
            if(i+1!=params['freq'].size):
                freq += ","
        dialog.frequencies_input.setText(freq)
        dialog.phase_offset_input.setText(str(params['phase']))
    elif isinstance(dialog, Ui_NoiseDialog):
        dialog.noise_type_input.setCurrentText(params['noise_type'])
        dialog.mean_value_input.setText(str(params['mean']))
        dialog.deviation_value_input.setText(str(params['devition']))
    elif isinstance(dialog, Ui_SweepDialog):
        dialog.sweep_method_input.setCurrentText(params['sweep_method'])
        dialog.f0_input.setText(str(params['f0']))
        dialog.f1_input.setText(str(params['f1']))


def makeMatplotLibGraph(group):
    figure = Figure()
    ax = figure.add_subplot(111)
    canvas = FigureCanvas(figure)
    toolbar = NavigationToolbar(canvas, group)
    layout = qtw.QVBoxLayout()
    layout.addWidget(toolbar)
    layout.addWidget(canvas)
    group.setLayout(layout)
    return [canvas, figure, ax, toolbar]

def getChildren(form, type="QCheckBox"):
    return form.findChildren(getattr(qtw,type))

class MainWindowWrapper(qtw.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindowWrapper, self).__init__(parent)
        self.main = Ui_MainWindow()
        self.main.setupUi(self)

    def getObject(self):
        return self.main

    def addSignalRow(self):
        pass
        # self.main.signal_gridlayout.addWidget(qtw.QLineEdit("Signal" + str(id)), id+3, 0)
        # self.main.signal_gridlayout.addWidget(qtw.QComboBox("Signal" + str(id)), id+3, 1)
        # self.main.signal_gridlayout.addWidget(qtw.QLineEdit("Signal" + str(id)), id+3, 2)

class WindowWrapper(qtw.QDialog):
    def __init__(self, dialog_obj, parent=None):
        super(WindowWrapper, self).__init__(parent)
        self.main = dialog_obj()
        self.main.setupUi(self)
    def getObject(self):
        return self.main
