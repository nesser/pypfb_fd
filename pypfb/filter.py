import numpy as np
import math
import scipy.signal as ss
import matplotlib.pyplot as plt
from scipy.signal import firwin, freqz, lfilter

class AnalysisFilter():

    def __init__(self, **kwargs):
        self.params = {}
        self.update(kwargs)


    def update(self, kwargs):
        if 'kwargs' in kwargs:
            kwargs = kwargs['kwargs']
        self._ntaps = kwargs.get('taps', 2)
        self._nchan = kwargs.get('channels', 32)
        self.bw_transition = kwargs.get('bw_transition', 4)
        self._window = kwargs.get('window', "rectangular")
        self._numerator = kwargs.get('pn', 1)
        self._denominator = kwargs.get('pd', 1)
        self._filter = kwargs.get('coeff', np.array([]))
        self.pass_min = kwargs.get('pass_min', 1/np.sqrt(2))
        self.stop_max = kwargs.get('stop_max', 0.001)
        self._rotation = 0
        self.os_factor = self._numerator / self._denominator
        self._tap_len = int((self._nchan-1)* 2) + 1
        self._filter_len = self._tap_len * self._ntaps
        self._chan_bw = 1/self._nchan
        self._step = self._denominator * self._tap_len // self._numerator
        self._filter = self.generateFilter()
        self.x_points, f_resp, self.p_resp = self.response(f0=1, f1=2, points=1000)
        self.f_resp = np.squeeze(f_resp)
        self.channelParams()

    def __str__(self):
        s = "Taps: {}\n".format(self._ntaps)
        s += "channels: {}\n".format(self._nchan)
        s += "window: {}\n".format(self._window)
        s += "pn: {}\n".format(self._numerator)
        s += "pd: {}\n".format(self._denominator)
        s += "tap_length: {}\n".format(self._tap_len)
        s += "self._filter_len: {}".format(self._filter_len)
        return s

    def generateFilter(self, norm=True):
        if norm:
            coeff = ss.firwin(self._filter_len, cutoff=self._numerator/(self._denominator*self._tap_len), window=self._window, scale=True).reshape(self._ntaps, self._tap_len).T
            return 1/np.max(coeff)*coeff
        else:
            return ss.firwin(self._filter_len, cutoff=self._numerator/(self._denominator*self._tap_len), window=self._window, scale=True).reshape(self._ntaps, self._tap_len).T

    def _pad(self, x):
        pad = 0
        if x.size < self._filter_len:
            pad = int(self._filter_len - x.size)
        elif x.size % self._filter_len:
            pad = x.size % self._filter_len
        return np.pad(x, (0, pad,), 'constant')

    def frontend(self, x):
        # x = self._pad(x)
        step = int(self._nchan * 2 / self.os_factor)
        y = []
        self._rot = 0
        offset = 0
        while True:
            start = offset * step
            end = start + self._ntaps * self._tap_len
            if end > x.size:
                break
            x_p = x[start:end].reshape(self._ntaps, self._tap_len).T
            sum = np.roll((x_p * self._filter),-self._rot).sum(axis=1)
            self._rot = (self._rot + (self._nchan * 2-step)) % self._nchan * 2
            y.append(sum)
            offset+=1
        y = np.asarray(y)
        return y

    def filter(self, x, filter=True):
        x_fir = self.frontend(x)
        return np.fft.rfft(x_fir, axis=1)

    def spectrum(self, x, n_int=1):
        y_pfb = self.filter(x)[:,:self._nchan]
        if n_int == "full":
            n_int = y_pfb.shape[0]
        y_psd = np.abs(y_pfb)**2
        y_phase = np.angle(y_pfb)
        y_psd = y_psd[:np.round(y_psd.shape[0]//n_int)*n_int]
        y_psd = y_psd.reshape(y_psd.shape[0]//n_int, n_int, y_psd.shape[1])
        y_psd = y_psd.mean(axis=1).flatten()
        return y_psd, y_phase, y_pfb


    def response(self, f0=0, f1=None, points=1000, n_int=1, full=False):
        if f1==None:
            f1 = self._nchan
        if full:
            period = np.linspace(0, 1, points)
        else:
            period = np.linspace((f0-1)*self._chan_bw, (f1)*self._chan_bw, points)
        t = np.arange(0, self._filter_len*n_int)
        f_resp = []
        p_resp = []
        for i, p in enumerate(period):
            x = np.sin(t * np.pi*p)
            psd, phase, pfb = self.spectrum(x, n_int=n_int)
            f_resp.append(psd)
            p_resp.append(phase)

        f_resp = np.asarray(f_resp)[:,f0:f1]
        p_resp = np.asarray(p_resp).reshape(points, self._nchan)[:,f0:f1]
        f_resp = np.squeeze(f_resp)*1/(2*np.max(f_resp))*2
        return period, f_resp, p_resp

    def channelParams(self, f_resp=None, db=False):
        if not f_resp:
            f_resp = self.f_resp

        self.passband = np.argwhere(f_resp > self.pass_min*np.max(self.f_resp)).squeeze()
        self.stopband = np.argwhere(f_resp < self.stop_max).squeeze()
        self.transition = np.where(((f_resp > self.stop_max) & (f_resp < self.pass_min)))[0]
        self.params["bw_pass"] = self.x_points[self.passband[-1]] - self.x_points[self.passband[0]]
        self.params["bw_trans"] = (self.x_points[self.transition[-1]] - self.x_points[self.transition[0]] - self.params["bw_pass"]) / 2
        self.params["ripple_pass"] = 20*np.log10(np.min(f_resp[self.passband]))
        self.params["ripple_stop"] = 20*np.log10(np.min(f_resp[self.stopband[1:]])) + 20*np.log10(self.stop_max)




class SynthesisFilter:

    def __init__(self, filter):
        self.update(filter)


    def update(self, filter):
        self._filter = filter
        self._numerator = filter._numerator
        self._denominator = filter._denominator
        self._multiplier = 1
        self._coeff = filter._filter
        self._nchan = filter._nchan
        self._ntaps = filter._ntaps
        self._os_ratio = float(self._numerator) / self._denominator
        if self._numerator == self._denominator:
            self._l = 0
        else:
            self._l = ((self._numerator - self._denominator) // 2 + 1) * self._multiplier
            self._r = (self._numerator - (self._numerator - self._denominator) // 2) * self._multiplier

    def filter(self, x, ifft=False):
        time = x.shape[0]
        chan = x.shape[1]
        # CS PFB
        if self._numerator == self._denominator:
            self._r = time
            fft_len = self._r
        else:
            fft_len = self._numerator * self._multiplier
        out1 = []
        for i, t in enumerate(range(time//(fft_len))):
            start = t * fft_len
            stop = start + fft_len
            y = x[start:stop,:]
            # y = np.fft.rfft(x[start:stop,:], axis=0)
            out1.append(np.fft.fftshift(np.fft.fftshift(y)[self._l:self._r,:]))
        if ifft:
            return np.fft.irfft(np.asarray(out1))
        else:
            return (np.asarray(out1))


    def spectrum(self, x, n_int=1):
        y = self._filter.filter(x)
        y = self.filter(y)
        y = y.reshape(y.shape[0]*y.shape[1], y.shape[2])
        y_psd = np.abs(y)**2
        y_phase = np.angle(y)
        y_psd = y_psd.mean(axis=0).flatten()
        return y_psd, y_phase, y


    def response(self, f0=0, f1=None, points=1000, n_int=1, full=False):
        if f1==None:
            f1 = self._nchan
        if full:
            period = np.linspace(0, 1, points)
        else:
            period = np.linspace((f0-1)*self._filter._chan_bw, (f1)*self._filter._chan_bw, points)
        t = np.arange(0, self._filter._filter_len*self._os_ratio*n_int)
        f_resp = np.zeros((points, f1-f0))
        p_resp = []
        for i, p in enumerate(period):
            x = np.sin(t * np.pi*p)
            psd, phase, pfb = self.spectrum(x)
            f_resp[i, :] = psd[f0:f1]
        f_resp = np.asarray(f_resp)#[:,f0:f1]
        return period, f_resp


def findFirstMaximum(x, start=0, stop=0):
    if stop == 0:
        stop = x.size-1
    e = x[start]
    for idx in range(start, stop):
        if x[idx] < e:
            return idx
    return idx
