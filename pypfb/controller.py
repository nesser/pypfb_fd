from PyQt5.QtWidgets import QApplication
from pypfb.gui import MainWindowWrapper, WindowWrapper, createDialog, makeMatplotLibGraph, getChildren, getDialogParams, setDialogParams
from pypfb.generator import SignalGenerator, additiveSignals
from pypfb.filter import AnalysisFilter, SynthesisFilter

import sys
import os
import numpy as np

MAX_N_SIGNALS = 4

class Controller():

    def __init__(self):
        # Init QTApplication
        self.app = QApplication(sys.argv)
        self.main_window = MainWindowWrapper()
        self.mw_obj = self.main_window.getObject()
        # Init matplot graphs
        self.freq_resp_graph = makeMatplotLibGraph(self.mw_obj.frequency_repsonse_groupBox)
        self.single_channel_graph = makeMatplotLibGraph(self.mw_obj.single_channel_groupBox)
        self.filter_frontend_graph = makeMatplotLibGraph(self.mw_obj.frontend_filter_groupBox)
        self.input_signal_td_graph = makeMatplotLibGraph(self.mw_obj.input_signal_td_groupBox)
        self.input_signal_fd_graph = makeMatplotLibGraph(self.mw_obj.input_signal_fd_groupBox)
        self.output_signal_time_freq_graph = makeMatplotLibGraph(self.mw_obj.output_signal_time_freq_groupBox)
        self.output_signal_psd_graph = makeMatplotLibGraph(self.mw_obj.output_signal_psd_groupBox)
        self.syn_signal_time_graph = makeMatplotLibGraph(self.mw_obj.syn_signal_time_groupBox)
        self.syn_signal_freq_graph = makeMatplotLibGraph(self.mw_obj.syn_signal_freq_groupBox)

        self.single_channel_graph[2] = [self.single_channel_graph[2]]
        self.single_channel_graph[2].append(self.single_channel_graph[2][0].twinx())

        self.file_dialog = createDialog("file_dialog")
        # Init Signal tab
        self.signal_add_buttons = getChildren(self.mw_obj.groupBox_4,"QCheckBox")
        self.signal_dialog_buttons = getChildren(self.mw_obj.groupBox_4,"QToolButton")
        self.signal_type_boxes = getChildren(self.mw_obj.groupBox_4,"QComboBox")
        self.signal_generators = [SignalGenerator() for x in range(len(self.signal_add_buttons))]
        self.signal_generators[0].setUsed(True)
        self.updateSignalGenerator(0, getDialogParams(self.signal_type_boxes[0].currentText()))
        self.pfb = AnalysisFilter()
        self.syn = SynthesisFilter(self.pfb)
        # Init PFB Specs tab
        self.setOSFactor(float(self.mw_obj.pnumerator_input.text())/float(self.mw_obj.pdivisor_input.text()))
        # --------------------#
        #  Connecting widgets #
        # --------------------#
        # Signal Generation connects
        self.signal_type_boxes = sorted(self.signal_type_boxes, key=lambda x: x.objectName())
        self.signal_add_buttons = sorted(self.signal_add_buttons, key=lambda x: x.objectName())
        self.signal_dialog_buttons = sorted(self.signal_dialog_buttons, key=lambda x: x.objectName())
        self.signal_add_buttons[0].clicked.connect(lambda: self.setSignal(0, self.signal_add_buttons[0].isChecked()))
        self.signal_add_buttons[1].clicked.connect(lambda: self.setSignal(1, self.signal_add_buttons[1].isChecked()))
        self.signal_add_buttons[2].clicked.connect(lambda: self.setSignal(2, self.signal_add_buttons[2].isChecked()))
        self.signal_add_buttons[3].clicked.connect(lambda: self.setSignal(3, self.signal_add_buttons[3].isChecked()))
        self.signal_dialog_buttons[0].clicked.connect(lambda: self.showDialog(0))
        self.signal_dialog_buttons[1].clicked.connect(lambda: self.showDialog(1))
        self.signal_dialog_buttons[2].clicked.connect(lambda: self.showDialog(2))
        self.signal_dialog_buttons[3].clicked.connect(lambda: self.showDialog(3))
        # File Dialog connects
        self.mw_obj.file_dialog_button.clicked.connect(self.file_dialog.show)
        self.file_dialog.fileSelected.connect(lambda: self.mw_obj.filter_fname_input.setText(self.file_dialog.getOpenFileName()[0]))
        # Common button connects
        self.mw_obj.signal_plot_button.clicked.connect(self.plotButtonClicked)
        self.mw_obj.generate_button.clicked.connect(self.generateButtonClicked)
        self.mw_obj.clear_button.clicked.connect(self.clearButtonClicked)
        self.mw_obj.logscale_check_input.stateChanged.connect(self.logScaleBoxStateChanged)
        self.mw_obj.optimum_check_input.stateChanged.connect(self.optimumBoxStateChanged)
        # Input fields (QLineEdit)
        self.mw_obj.pnumerator_input.editingFinished.connect(lambda: self.setOSFactor(float(self.mw_obj.pnumerator_input.text())/float(self.mw_obj.pdivisor_input.text())))
        self.mw_obj.pdivisor_input.editingFinished.connect(lambda: self.setOSFactor(float(self.mw_obj.pnumerator_input.text())/float(self.mw_obj.pdivisor_input.text())))
        self.generateButtonClicked()

    def execute(self):
        return self.app.exec_()

    # ---------------------- #
    #  Window show function  #
    # ---------------------- #
    def showMainWindow(self):
        self.main_window.show()

    def showDialog(self, id):
        self.dialog = createDialog(self.signal_type_boxes[id].currentText())
        # if self.signal_generators[id].used:
        setDialogParams(self.dialog.main, self.signal_generators[id].getParams())
        self.dialog.main.apply_button.clicked.connect(lambda: self.updateSignalGenerator(id, getDialogParams(self.dialog.main)))
        self.dialog.main.apply_button.clicked.connect(self.dialog.close)
        self.dialog.main.cancel_button.clicked.connect(self.dialog.close)
        self.dialog.show()

    # ----------------- #
    #  Setter function  #
    # ----------------- #
    def setSignal(self, id, val):
        self.signal_generators[id].used = val

    def setOSFactor(self, val):
        self.mw_obj.oversampling_factor_input.setText(str(val))

    # ----------------- #
    # Updating function #
    # ----------------- #
    def updateSignalGenerator(self, id, params):
        params["fs"] = float(self.mw_obj.sample_freq_input.text())
        params["duration"] = float(self.mw_obj.duration_input.text())
        params["type"] = self.signal_type_boxes[id].currentText()
        print(params)
        self.signal_generators[id].update(params)
        self.signal_generators[id].output = 0

    def updatePFB(self):
        params = {}
        params["taps"] = int(self.mw_obj.n_taps_input.text())
        params["channels"] = int(self.mw_obj.n_channels_input.text())
        params["pn"] = int(self.mw_obj.pnumerator_input.text())
        params["pd"] = int(self.mw_obj.pdivisor_input.text())
        params["window"] = self.mw_obj.window_fn_input.currentText().lower()
        self.pfb.update(params)
        for key, val in self.pfb.params.items():
            try:
                getattr(self.mw_obj, key).setText(str(val))
            except:
                print("{} is not an attribute".format(key))

    def updateSingleChannelGraph(self):
        f_resp = self.pfb.f_resp
        p_resp = self.pfb.p_resp
        x_points = self.pfb.x_points
        l_stop = np.argwhere(self.pfb.stopband < self.pfb.passband[0]).squeeze()
        r_stop = np.argwhere(self.pfb.stopband > self.pfb.passband[-1]).squeeze()

        self.single_channel_graph[2][0].axvline(x_points[self.pfb.passband[0]], linestyle='--')
        self.single_channel_graph[2][0].axvline(x_points[self.pfb.passband[-1]], linestyle='--')
        try:
            self.single_channel_graph[2][0].axvline(x_points[self.pfb.stopband[l_stop[-1]]], linestyle='-')
            self.single_channel_graph[2][0].axvline(x_points[self.pfb.stopband[r_stop[0]]], linestyle='-')
        except:
            pass
        updateXYYPlot(self.single_channel_graph, x_points, [f_resp, p_resp], ylabel=["Magnitude","Angle [rad]"], linestyle=['-', 'dashdot'], linewidth=[1, 0.3])



    def updateInputSignalTab(self):
        fs = float(self.mw_obj.sample_freq_input.text())
        x_time, y_time = additiveSignals(self.signal_generators)
        y_fft = np.abs(np.fft.fft(y_time)[:int(y_time.size/2)])
        x_fft = np.linspace(0, fs/2, y_fft.size)
        updateXYPlot(self.input_signal_td_graph, x_time, y_time)
        updateXYPlot(self.input_signal_fd_graph, x_fft, y_fft)

    def updateSynthesizedSignalTab(self):
        # if self.pfb._numerator != 1 and self.pfb._denominator != 1:
        fs = float(self.mw_obj.sample_freq_input.text())
        self.syn.update(self.pfb)
        x_time, y_time = additiveSignals(self.signal_generators)
        x = self.syn.filter(self.o_pfb, ifft=True)
        x = x.flatten()
        x_time = np.arange(0, x.size)
        y_fft = np.abs(np.fft.fft(x))
        x_fft = np.linspace(0, fs/2, y_fft.size)
        updateXYPlot(self.syn_signal_time_graph, x_time, x)
        updateXYPlot(self.syn_signal_freq_graph, x_fft, y_fft)

    def updateOutputSignalTab(self):
        x_time, y_time = additiveSignals(self.signal_generators)
        psd, phase, self.o_pfb = self.pfb.spectrum(y_time, n_int="full")
        update2DPlot(self.output_signal_time_freq_graph,  20*np.log10(np.abs(self.o_pfb)))
        updateXYPlot(self.output_signal_psd_graph, np.arange(0,self.pfb._nchan),(psd)**2)

    def updateFilterResponseTab(self):
        f0 = int(self.mw_obj.channel_start_input.text())
        f1 = int(self.mw_obj.channel_stop_input.text())+1
        self.omega_norm, self.y_psd, self.y_phase = self.pfb.response(
            f0=f0, f1=f1,
            points=int(self.mw_obj.resolution_point_input.text()),
            n_int=1,
            full=self.mw_obj.fullfreq_check_input.isChecked())

        if self.mw_obj.logscale_check_input.isChecked():
            y_psd = 20*np.log(self.y_psd)
        else:
            y_psd = self.y_psd

        for chan in range(self.y_psd.shape[1]):
            updateXYPlot(self.freq_resp_graph, self.omega_norm, y_psd[:,chan], xlabel="Normalized frequency", ylabel="Magnitude", xlim=[(f0-1)/self.pfb._nchan, (f1+1)/self.pfb._nchan])
        self.updateSingleChannelGraph()

    def updateFrontendFilterTab(self):
        updateXYPlot(self.filter_frontend_graph, np.arange(self.pfb._filter_len), self.pfb._filter.T.flatten())


    # -------------------- #
    # User event functions #
    # -------------------- #
    def plotButtonClicked(self):
        self.updatePFB()
        self.updateInputSignalTab()
        self.updateOutputSignalTab()
        self.updateSynthesizedSignalTab()

    def generateButtonClicked(self):
        self.updatePFB()
        self.updateFilterResponseTab()
        self.updateFrontendFilterTab()
        self.updateInputSignalTab()
        self.updateOutputSignalTab()
        self.updateSynthesizedSignalTab()

    def clearButtonClicked(self):
        self.clearSignals()
        clearPlot(self.freq_resp_graph)
        clearPlot(self.filter_frontend_graph)
        clearPlot(self.single_channel_graph)
        clearPlot(self.input_signal_td_graph)
        clearPlot(self.input_signal_fd_graph)
        clearPlot(self.output_signal_time_freq_graph)
        clearPlot(self.output_signal_psd_graph)
        clearPlot(self.syn_signal_time_graph)
        clearPlot(self.syn_signal_freq_graph)

    def logScaleBoxStateChanged(self):
        f0 = int(self.mw_obj.channel_start_input.text())
        f1 = int(self.mw_obj.channel_stop_input.text())+1
        clearPlot(self.freq_resp_graph)
        for chan in range(self.y_psd.shape[1]):
            if self.mw_obj.logscale_check_input.isChecked():
                updateXYPlot(self.freq_resp_graph, self.omega_norm, 20*np.log(self.y_psd[:,chan]), xlabel="Normalized frequency", ylabel="Magnitude [dB]", xlim=[(f0-1)/self.pfb._nchan, (f1+1)/self.pfb._nchan])
            else:
                updateXYPlot(self.freq_resp_graph, self.omega_norm, self.y_psd[:,chan], xlabel="Normalized frequency", ylabel="Magnitude", xlim=[(f0-1)/self.pfb._nchan, (f1+1)/self.pfb._nchan])

    def optimumBoxStateChanged(self):
        chan_bw = 1/self.pfb._nchan
        ideal_filter = np.zeros(self.omega_norm.size)
        f0 = int(self.mw_obj.channel_start_input.text())
        f1 = int(self.mw_obj.channel_stop_input.text())+1
        samples_per_channel = self.omega_norm.size/(f1-f0+1)
        for chan in range(f0, f1):
            if chan == f0:
                start = int(samples_per_channel * (chan) - samples_per_channel/2)
            else:
                start = stop + 1
            stop = int(start + samples_per_channel)
            ideal_filter[start:stop] = 1
        updateXYPlot(self.freq_resp_graph, self.omega_norm, ideal_filter, xlabel="Normalized frequency", ylabel="Magnitude [dB]", xlim=[(f0-1)/self.pfb._nchan, (f1+1)/self.pfb._nchan], linestyle="--", color="black")


    def clearSignals(self):
        self.omega_norm = 0
        self.y_psd = 0
        self.y_phase = 0




def updateXYPlot(graph, x,y,xlabel=None,ylabel=None,xlim=[None, None],ylim=[None, None], log=False, linewidth=None, linestyle=None, color=None):
    canvas = graph[0]
    figure = graph[1]
    ax = graph[2]
    if log == True:
        y = 20*np.log(y)
    ax.plot(x,y, linestyle=linestyle, c=color,linewidth=linewidth)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    canvas.draw()

def updateXYYPlot(graph, x,y,xlabel=None,ylabel=None,xlim=[None, None],ylim=[None, None], log=False, linewidth=[None, None], linestyle=[None, None], color=[None, None]):
    canvas = graph[0]
    figure = graph[1]
    if log == True:
        y = 20*np.log(y)
    for i, axis in enumerate(graph[2]):
        axis.plot(x,y[i], linestyle=linestyle[i], c=color[i],linewidth=linewidth[i])
        axis.set_xlabel(xlabel)
        axis.set_ylabel(ylabel[i])
        axis.set_xlim(xlim)
        axis.set_ylim(ylim)
    canvas.draw()

def update2DPlot(graph, x, xlabel=None,ylabel=None,xlim=[None, None],ylim=[None, None], log=False,):
    canvas = graph[0]
    figure = graph[1]
    ax = graph[2]
    if log == True:
        ax.imshow(x, interpolation='nearest', aspect="auto")
    else:
        ax.imshow(x, interpolation='nearest', aspect="auto")
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    canvas.draw()

def clearPlot(graph):
    canvas = graph[0]
    figure = graph[1]
    if isinstance(graph[2], list):
        for axis in graph[2]:
            axis.clear()
    else:
        graph[2].clear()
    canvas.draw()
